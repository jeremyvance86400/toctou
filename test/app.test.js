const request = require('supertest');
const { app } = require('./app');
const TransferService = require('./classes/TransferService');
const Amount = require('./classes/Amount');
const Balance = require('./classes/Balance');
const Transfer = require('./classes/Transfer');

describe('usability', () => {

    it('Request to transfer 500, should return successfully', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: 500});
        expect(res.statusCode).toEqual(200);
        expect(res.text).toContain('Successfully transfered: 500. Your balance: 500');
    });

    it('Request to transfer 1001, should fail with 400', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: 1001});
        expect(res.statusCode).toEqual(400);
    });

    it('when request /status, should return 200', async () => {
        const res = await request(app)
            .get('/status');
        expect(res.statusCode).toEqual(200);
    });

    it('should instantiate the correct classes', async() => {
        const service = new TransferService();
        service.createTransfer(100);
        expect(service._amount).toBeInstanceOf(Amount);
        expect(service._baseBalance).toBeInstanceOf(Balance);
        expect(service._transfer).toBeInstanceOf(Transfer);
    })
});