const request = require('supertest');
const { app } = require('./app');
const TransferService = require('./classes/TransferService');

describe('security', () => {

    it('Request for negative amount, should return 400', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: -500});
        expect(res.statusCode).toEqual(400);
    });

    it('Request to transfer non-numeric value should fail with 400', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: 'blabla'});
        expect(res.statusCode).toEqual(400);
    });

    it('Request to transfer suspicious payload should fail with 400', async () => {
        const res = await request(app)
            .get('/')
            .query({amount: '100%3Famount=1000'});
        expect(res.statusCode).toEqual(400);
    })

    it('TOCTOU check 505 use 2000, should succesffuly process 505', async () => {
        const payload = {
            checked: 0,
            toString() {
                if (this.checked >= 1) {
                    return 2000
                }
                this.checked = 1
                return 505
            }
        };
        const transfer = new TransferService().createTransfer(payload);
        expect(transfer).toEqual({ balance: 495, transfered: 505 });
    });
});
