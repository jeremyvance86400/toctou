'use strict';

// requirements
const express = require('express');

const TransferService = require('./classes/TransferService');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();


// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => { 
    try {
        const transfer = new TransferService().createTransfer(req.query.amount);
        res.status(200).end('Successfully transfered: ' + transfer.transfered + '. Your balance: ' + transfer.balance);
    } catch(error) {
        res.status(400).end(error.message);
    }
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app };
