class InsufficientFundsException extends Error {
    constructor(message) {
        super(message);
        this.name = "InsufficientFundsException";
    }
}

module.exports = InsufficientFundsException;