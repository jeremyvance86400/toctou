const Amount = require('./Amount');
const Balance = require('./Balance');
const Transfer = require('./Transfer');

class TransferService {
    constructor() {}

    createTransfer(amount) {
        this._amount = new Amount(amount);
        this._baseBalance = new Balance(1000);
        this._transfer = new Transfer(this._baseBalance.balance, this._amount.amount);
        
        return this._transfer.transfer;
    }
}

module.exports = TransferService;