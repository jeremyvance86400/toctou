class TransferRecord {
    constructor(amount) {
        this._amount = amount;
    }

    get amount() {
        return this._amount;
    } 
}

module.exports = TransferRecord;