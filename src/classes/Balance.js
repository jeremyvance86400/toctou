class Balance {
    constructor(balance) {
        this._balance = balance;
        Object.freeze(this);
    }

    get balance() {
        return this._balance;
    }
}

module.exports = Balance;