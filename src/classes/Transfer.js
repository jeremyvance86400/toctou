const Balance = require('../classes/Balance');
const TransferRecord = require('../classes/TransferRecord');
const InsufficientFundsException = require('./Exceptions/InsufficientFundsException');

class Transfer {
    constructor(balance, amount) {
        this._balance = balance;
        this._amount = amount;

        this.process(this._balance, this._amount);
        Object.freeze(this);
    }

    get transfer() {
        return { balance: this._remaining.balance, transfered: this._transfered.amount };
    }

    process(balance, amount) {
        const _balance = Number(balance);
        const _amount = Number(amount);
        const _calculated = _balance - _amount;
        if (_calculated >= 0) {
            this._remaining = new Balance(_calculated);
            this._transfered = new TransferRecord(_amount);
        } else {
            throw new InsufficientFundsException(`Insufficient funds. Your balance: ${_balance}`);
        }
    }
}

module.exports = Transfer;