const InvalidAmountException = require("./Exceptions/InvalidAmountException");

class Amount {    
    constructor(amount) {
        this._amount = this.validate(amount);
        Object.freeze(this);
    }

    get amount() {
        return this._amount;
    }

    validate(amount) {
        const parsed = Number(amount);

        if (Number.isInteger(parsed) &&
            parsed > 0 &&
            parsed <= Math.pow(2, 32)/2 - 1
        ) {
            return parsed;
        } else {
            throw new InvalidAmountException(`${parsed} is not a valid amount.`)
        }
    }
}

module.exports = Amount;